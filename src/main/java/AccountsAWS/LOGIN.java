package AccountsAWS;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LOGIN {
	
	
	
	WebDriver driver;
	
	
	public LOGIN(WebDriver driver) {
		
		this.driver = driver;
	}
	
	@FindBy(how=How.XPATH,using="//*[@id=\"login-form\"]/div/div[1]/input") WebElement username;
	@FindBy(how=How.NAME,using="password") WebElement password;
	@FindBy(how=How.XPATH,using="//*[@id=\"login-form\"]/button") WebElement loginButton;
	@FindBy(how=How.XPATH, using="//*[@id=\"nav\"]/div/div/a[1]") WebElement firstLoginButton;
	
	
	public void FirstLoginButtonClick()
	{
		firstLoginButton.click();
	}
	
	public void TypeUserName(String uname) {
		username.sendKeys(uname);
	}	
	public void TypePassword(String pwd) {
		password.sendKeys(pwd);
	}	
	
	public void clickLoginButton() {		
		loginButton.click();
	}
	public void Login(String uname,String pwd) {
		TypeUserName(uname);
		TypePassword(pwd);
		clickLoginButton();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
