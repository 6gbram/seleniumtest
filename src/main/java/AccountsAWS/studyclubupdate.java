package AccountsAWS;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class studyclubupdate {

	WebDriver driver;
	
	
	public studyclubupdate(WebDriver driver) {
		
		this.driver = driver;
	}
	
	@FindBy(how=How.XPATH,using="//*[@id=\"adminbar\"]/ul/li[7]/a") WebElement studyclub;
	@FindBy(how=How.XPATH,using="//*[@id=\"adminbar\"]/ul/li[7]/ul/li[3]/a") WebElement clubs;
	@FindBy(how=How.XPATH,using="//*[@id=\"list\"]/div[2]/table/tbody/tr") List<WebElement> clubslist;
	@FindBy(how=How.XPATH,using="//*[@id=\"update-club\"]") WebElement updateclub;
	@FindBy(how=How.CSS,using=".alert.alert-dismissable.alert-danger") WebElement Errormessage;
	@FindBy(how=How.CLASS_NAME,using="alert alert-dismissable alert-success") WebElement successmessage;
	//@FindBy(how=How.CLASS_NAME,using="table table-bordered table-striped table-hover") WebElement clubsTable;
	
	public void clickonstudyclub() {
	studyclub.click();
	}
	public void clickonclubs() {
		clubs.click();
		}
	public void selectaclub() {
		WebElement clubsTable = (new WebDriverWait(driver, 10))
				   .until(ExpectedConditions.visibilityOfElementLocated(By.className("table table-bordered table-striped table-hover")));
	driver.findElements(By.xpath("//*[@id=\\\"list\\\"]/div[2]/table/tbody/tr")).get(0).click();
		}

public void clickonupdateclub() {
	updateclub.click();
	}
public void checkthemessage() {
	Assert.assertFalse(Errormessage.isDisplayed(), "Error message was displayed");
	Assert.assertEquals(successmessage.isDisplayed(), true,"Successmessage was not displayed");
	}
}