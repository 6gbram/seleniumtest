package AccountsAWS;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class PAGEVER {
WebDriver driver;
	
	
	public PAGEVER(WebDriver driver) {
		
		this.driver = driver;
		
	}
	
	@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul/li[1]/a") WebElement campuslearning;
	@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul/li[2]/a") WebElement OnlineEducation;
	@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul/li[3]/a") WebElement patientEducation;
	@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul/li[4]/a") WebElement TeamEducation;
	@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul/li[5]/a") WebElement studyclub;
	@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul/li[6]/a") WebElement Practiceconsulting;
	@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul/li[7]/a") WebElement SpearFaculty;
	@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul/li[8]/a") WebElement SpearDigest;

public void campuslearninglinkcheck() {
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = campuslearning.getAttribute("href");
	//Assert.assertEquals(campuslearning.isDisplayed(), true,"my button was not displayed");//
	Assert.assertEquals(link.toLowerCase(), "https://www.speareducation.net/seminars", "expected is https://www.speareducation.net/seminars, got: "+link);
}
public void OnlineEducationlinkcheck() {
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = OnlineEducation.getAttribute("href");
	//Assert.assertEquals(OnlineEducation.isDisplayed(), true,"my button was not displayed");//
	Assert.assertEquals(link.toLowerCase(), "https://www.speareducation.net/digital-suite", "expected is https://www.speareducation.net/digital-suite, got: "+link);
}

public void patientEducationlinkcheck() {
	
	
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = patientEducation.getAttribute("href");
	//Assert.assertEquals(patientEducation.isDisplayed(), true,"my button was not displayed");//
	Assert.assertEquals(link.toLowerCase(), "https://www.speareducation.net/patient-education", "expected is https://www.speareducation.net/patient-education, got: "+link);
}
public void TeamEducationlinkcheck() {
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = TeamEducation.getAttribute("href");
	//Assert.assertEquals(TeamEducation.isDisplayed(), true,"my button was not displayed");//
	Assert.assertEquals(link.toLowerCase(), "https://www.speareducation.net/team-education", "expected is https://www.speareducation.net/team-education, got: "+link);
}


public void studyclublinkcheck() {
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = studyclub.getAttribute("href");
	//Assert.assertEquals(studyclub.isDisplayed(), true,"my button was not displayed");//
	Assert.assertEquals(link.toLowerCase(), "https://www.speareducation.net/study-club", "expected is https://www.speareducation.net/study-club, got: "+link);
}

public void Practiceconsultinglinkcheck() {
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = Practiceconsulting.getAttribute("href");
	//Assert.assertEquals(Practiceconsulting.isDisplayed(), true,"my button was not displayed");//
	Assert.assertEquals(link.toLowerCase(), "https://www.speareducation.net/practice-solutions", "expected is https://www.speareducation.net/practice-solutions, got: "+link);
}
public void SpearFacultylinkcheck() {
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = SpearFaculty.getAttribute("href");
	//Assert.assertEquals(SpearFaculty.isDisplayed(), true,"my button was not displayed");//
	Assert.assertEquals(link.toLowerCase(), "https://www.speareducation.net/faculty", "expected is https://www.speareducation.net/faculty, got: "+link);
}
public void SpearDigestlinkcheck() {
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = SpearDigest.getAttribute("href");
	//Assert.assertEquals(SpearDigest.isDisplayed(), true,"my button was not displayed");//
		Assert.assertEquals(link.toLowerCase(), "https://www.speareducation.net/spear-review", "expected is https://www.speareducation.net/spear-review, got: "+link);
	}
}

