package AccountsAWS;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class UserFlow {
	
	
	
	WebDriver driver;
	
	
	public UserFlow(WebDriver driver) {
		
		this.driver = driver;
	}
	
	@FindBy(how=How.XPATH,using="//*[@id=\"adminbar\"]/ul/li[11]/a") WebElement users;
	@FindBy(how=How.ID,using="user-search") WebElement searchUsers;
	@FindBy(how=How.ID,using="search-btn") WebElement searchButton;
	@FindBy(how=How.XPATH,using="//*[@id=\"list\"]/div[2]/table/tbody/tr/td[13]/a[4]") WebElement LoginAs ;
	
	public void ClickOnUsers() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		users.click();
	}
	
	public void SearchUserByID(String id) {
		searchUsers.sendKeys(id);
	}
	
	public void ClickSearchButton() {
		searchButton.click();
	}
	
	public void ClickLoginAsICON() {
		LoginAs.click();
		driver.switchTo().alert().accept();
	}

}
