package AccountsAWS;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class Newuser{
	
	
	WebDriver driver;
	
	
	public Newuser(WebDriver driver) {
		
		this.driver = driver;
	}
	
	@FindBy(how=How.XPATH,using="//*[@id=\"adminbar\"]/ul/li[11]/a") WebElement users;
	@FindBy(how=How.XPATH,using="//*[@id=\"wrap\"]/div[2]/div/div/div[2]/p/a[1]") WebElement Newuser;
	@FindBy(how=How.ID,using="user_first_name") WebElement firstname;
	@FindBy(how=How.ID,using="user_last_name") WebElement lastname;
	@FindBy(how=How.ID,using="user_email") WebElement useremail;
	@FindBy(how=How.ID,using="password") WebElement upassword;
	@FindBy(how=How.XPATH,using="//*[@id=\"manage\"]/div/button") WebElement submit;
	@FindBy(how=How.ID,using="flashmessenger") WebElement SuccessMessage;
	

	
	public void ClickOnUsers() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		users.click();
	}
	
	
	public void ClickOnNewuser() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Newuser.click();
	}

	public void firstnameByID(String id) {
		firstname.sendKeys(id);
	}
	
	public void lastnameById(String id) {
		lastname.sendKeys(id);
	}
	public void useremailById(String id) {
		useremail.sendKeys(id);
	}	
	
	public void upasswordById(String id) {
		upassword.sendKeys(id);
	}	
	public void ClickOnsubmit() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//submit.click();
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", submit);
		Assert.assertTrue(SuccessMessage.getText().contains(("success")),"Success message was not displayed");
	}
	
	}

	
	
