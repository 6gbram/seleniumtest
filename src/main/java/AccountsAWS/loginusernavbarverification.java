package AccountsAWS;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class loginusernavbarverification {
WebDriver driver;
	
	
	public loginusernavbarverification(WebDriver driver) {
		
		this.driver = driver;
	}
	
	@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul[2]/li[1]/a")
	static WebElement courselibrary;


public static void campuslearninglinkcheck() {
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = courselibrary.getAttribute("href");
	//Assert.assertEquals(courselibrary.isDisplayed(), true,"my button was not displayed");//
	Assert.assertEquals(link.toLowerCase(), "https://www.speareducation.net/digital-learning/course-library", "expected is https://www.speareducation.net/digital-learning/course-library, got: "+link);
}
}