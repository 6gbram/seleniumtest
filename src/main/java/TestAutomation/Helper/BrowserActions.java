package TestAutomation.Helper;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

public class BrowserActions {
	
	static WebDriver driver = null;
	
	public static WebDriver OpenBrowser(String browser,String url) {
		
		if(browser.equalsIgnoreCase("Firefox") || browser.equalsIgnoreCase("ff")) {
			driver = new FirefoxDriver();
		}
		
		if(browser.equalsIgnoreCase("Chrome") || browser.equalsIgnoreCase("gc")) {
			System.setProperty("webdriver.chrome.driver", "src\\resources\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		
		if(browser.equalsIgnoreCase("ie11") || browser.equalsIgnoreCase("InternetExplorer11")) {
			driver = new InternetExplorerDriver();
		}
		
		if(browser.equalsIgnoreCase("Edge")) {
			driver = new EdgeDriver();
		}
		
		if(driver == null) {
			driver = new ChromeDriver();
		}
		
		Navigate(url);
		
		driver.manage().window().maximize();
		
		return driver;
		
	}
	
	public static void CloseBrowser() {
		
		driver.close();
		
	}
	
	public static void Navigate(String url) {
		driver.get(url);
	}

	public static void WaitForElement(WebElement Element )
	{
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(Element));
	}
	
    public static void JavaSendKeys(WebElement element, String text)
    {
        ((JavascriptExecutor)driver).executeScript("arguments[0].value=(" + text + ");arguments[0].change;", element);
    }
}

