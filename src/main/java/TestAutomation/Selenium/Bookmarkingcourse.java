package TestAutomation.Selenium;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class Bookmarkingcourse {
	
WebDriver driver;
	
	
	public Bookmarkingcourse(WebDriver driver) {
		
		this.driver = driver;
	}

	
	@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul[2]/li[1]/a/i") WebElement onlineducation;
	@FindBy(how=How.ID,using="search-courses") WebElement coursesearch;
	@FindBy(how=How.XPATH,using="//*[@id=\"courses-list\"]/div[1]/div/figure/a[3]/div") WebElement Bookmark;
	@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul[2]/li[8]") WebElement Bookmarkdropdown;
	@FindBy(how=How.XPATH,using="//*[@id=\"bookmark-items\"]/a/div/div") WebElement Clickoncourses;
	//@FindBy(how=How.XPATH,using="//*[@id=\"bookmark-96912\"]/div[2]/p/text()") WebElement Displayedcourse;
	
	
	public void ClickOnonlineducation() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		onlineducation.click();
	}
	
	public void coursesearchByID(String id) {
		coursesearch.sendKeys(id);
	}

	public void clickOnBookmark() {
		//WebDriverWait wait = new WebDriverWait(driver, 60);
		//wait.until(ExpectedConditions.elementToBeClickable(Bookmark));
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", Bookmark);
		//Bookmark.click();
		
	}

	public void ClickOnBookamrkdropdown() {
	    Bookmarkdropdown.click();
	}
	
	public void ClickOnClickoncourses() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//submit.click();
		//((JavascriptExecutor)driver).executeScript("arguments[0].click();", Clickoncourses);
		//Assert.assertTrue(Displayedcourse.getText().contains(("Tooth Position")),"Success message was not displayed");
	}
		
	
	
}
