package TestAutomation.Selenium;

import org.apache.bcel.generic.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.sun.jna.Union;


public class Addaddress {

WebDriver driver;
	
	
	public Addaddress(WebDriver driver) {
		
		this.driver = driver;
	}
	
	@FindBy(how=How.XPATH,using="//*[@id=\"profile-icon\"]/img") WebElement avtar;
	//click on avtar
	@FindBy(how=How.XPATH,using="//*[@id=\"user-profile-left\"]/ul/li[4]/a") WebElement settings;
	//click on settings
	
	@FindBy(how=How.XPATH,using="//*[@id=\"settings-nav\"]/ul/li[3]/a") WebElement addressmanager;
	//click on address manager
	
	@FindBy(how=How.XPATH,using="//*[@id=\"settings-content\"]/div[1]/div[1]/a") WebElement addaddress;
	//click on add address
	
	@FindBy(how=How.ID,using="country") WebElement country;
	//Select select=new Select(driver.findElement(By.xpath(“//*[@id="country"]))Select.selectByValue(US));

	// select country from drop down
	@FindBy(how=How.ID,using="street_address") WebElement streetaddress;
	
	@FindBy(how=How.ID,using="city") WebElement city;
	
	@FindBy(how=How.ID,using="state") WebElement state;
	//select state from drop down
	
	@FindBy(how=How.ID,using="zip") WebElement postalcode;
	
	@FindBy(how=How.ID,using="save-address") WebElement saveaddress;
	
}
