package TestAutomation.Selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumDriver {

	
	 WebDriver driver;
	
	
	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public  void OpenBrowser(String browser) {
	   if(browser.equalsIgnoreCase("Chrome") || browser.equalsIgnoreCase("GC") || browser.equalsIgnoreCase("Google") ) {
		  System.setProperty("webdriver.chrome.driver", "src\\resources\\drivers\\chromedriver.exe");
		  driver = new ChromeDriver();
					 
	   }
	}
	
	public void CloseBrowser() {
		driver.quit();
	}
	
	public void NavigateToUrl(String url) {
		driver.get(url);
	}
}
