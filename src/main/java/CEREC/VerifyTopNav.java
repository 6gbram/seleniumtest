package CEREC;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class VerifyTopNav {
	WebDriver driver;
   
	
public VerifyTopNav(WebDriver driver) {
		
		this.driver = driver;
		
	}
@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul/li[1]/a") WebElement campuslearning;
@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul/li[2]/a") WebElement digitallearning;
@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul/li[3]/a") WebElement discussionboards;
@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul/li[4]/a") WebElement membershipoptions;
@FindBy(how=How.XPATH,using="//*[@id=\"nav\"]/div/ul/li[7]/a") WebElement academybenefits;

public void campuslearninglinkcheck() {
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = campuslearning.getAttribute("href");
	//Assert.assertEquals(campuslearning.isDisplayed(), true,"my button was not displayed");//
	Assert.assertEquals(link.toLowerCase(), "http://staging.www.cerecdoctors.co/campus-learning", "expected is http://staging.www.cerecdoctors.co/campus-learning, got: "+link);
}

public void digitallearningcheck() {
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = digitallearning.getAttribute("href");
	//Assert.assertEquals(campuslearning.isDisplayed(), true,"my button was not displayed");//
	Assert.assertEquals(link.toLowerCase(), "http://staging.www.cerecdoctors.co/digital-learning", "expected is http://staging.www.cerecdoctors.co/digital-learning, got: "+link);
}

public void discussionboardscheck() {
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = discussionboards.getAttribute("href");
	//Assert.assertEquals(campuslearning.isDisplayed(), true,"my button was not displayed");//
	Assert.assertEquals(link.toLowerCase(), "http://staging.www.cerecdoctors.co/discussion-boards", "expected is http://staging.www.cerecdoctors.co/discussion-boards, got: "+link);
}
public void membershipoptionscheck() {
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = membershipoptions.getAttribute("href");
	//Assert.assertEquals(campuslearning.isDisplayed(), true,"my button was not displayed");//
	Assert.assertEquals(link.toLowerCase(), "http://staging.www.cerecdoctors.co/upgrade", "expected is http://staging.www.cerecdoctors.co/upgrade, got: "+link);
}

public void academybenefitscheck() {
	try {
		Thread.sleep(1);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String link = academybenefits.getAttribute("href");
	//Assert.assertEquals(campuslearning.isDisplayed(), true,"my button was not displayed");//
	Assert.assertEquals(link.toLowerCase(), "http://staging.www.cerecdoctors.co/academy/curriculum-series", "expected is http://staging.www.cerecdoctors.co/academy/curriculum-series, got: "+link);
}
	}


