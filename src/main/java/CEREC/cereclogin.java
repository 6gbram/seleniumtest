package CEREC;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import TestAutomation.Helper.BrowserActions;

public class cereclogin {


	WebDriver driver;
public cereclogin(WebDriver driver) {
		
		this.driver = driver;
	}
	
	//@FindBy(how=How.XPATH,using="//*[@id=\"loginEmail\"]") 
	@FindBy(how=How.ID,using="loginEmail")
	static WebElement username;
	@FindBy(how=How.XPATH,using="//*[@id=\"loginPassword\"]")
	static WebElement password;
	@FindBy(how=How.XPATH,using="//*[@id=\"submitLogin\"]")
	static WebElement loginButton;
	@FindBy(how=How.XPATH, using="//*[@id=\"utility\"]/div/ul/li[1]/a") WebElement firstLoginButton;
	
	
	public void FirstLoginButtonClick()
	{
		firstLoginButton.click();
	}
	
	public void TypeUserName(String uname) {
		
		
		BrowserActions.JavaSendKeys(username, uname);
		
		username.click();
		username.sendKeys(uname);
	}	
	public void TypePassword(String pwd) {
		password.sendKeys(pwd);
	}	
	
	public void clickLoginButton() {		
		loginButton.click();
	}
	public void Login(String uname,String pwd) {
	
		TypeUserName(uname);
		TypePassword(pwd);
		clickLoginButton();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
