package TestAutomation.Selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import TestAutomation.Helper.BrowserActions;

public class VerifyLogInAsFuntionality {
	
	WebDriver driver;
	
	@Test
	public void  LogInAs() {
		
		driver =BrowserActions.OpenBrowser("GC", "https://www.speareducation.net//login");
		
		SpearLogin spearlogin = PageFactory.initElements(driver, SpearLogin.class);
		UserFlow userFlow = PageFactory.initElements(driver, UserFlow.class);
		
		spearlogin.Login("mkothi@speareducation.com", "Abinika@123");	
		
		userFlow.ClickOnUsers();
		userFlow.SearchUserByID("75108");
		userFlow.ClickSearchButton();
		userFlow.ClickLoginAsICON();
		
		BrowserActions.CloseBrowser();
		
	}

}
