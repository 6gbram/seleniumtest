package CERECSCRIPTSTC;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import CEREC.VerifyTopNav;
import TestAutomation.Helper.BrowserActions;

public class TCTopNavBar {
	
	WebDriver driver;
	@Test
	public void pageverification() {
		
		driver =BrowserActions.OpenBrowser("GC", "http://staging.www.cerecdoctors.co/");
		
		 VerifyTopNav  VerifyTopNav = PageFactory.initElements(driver,  VerifyTopNav.class);
		
		 VerifyTopNav.campuslearninglinkcheck();
		 VerifyTopNav.digitallearningcheck();
		 VerifyTopNav.discussionboardscheck();
		 VerifyTopNav.membershipoptionscheck();
		
		
		BrowserActions.CloseBrowser();
		
	}

}
