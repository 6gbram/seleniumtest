package TestscriptsAccountsAWS;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import AccountsAWS.UserFlow;
import TestAutomation.Helper.BrowserActions;
import TestAutomation.Selenium.SpearLogin;

public class VerifyLogInAsFuntionality {
	
	WebDriver driver;
	
	@Test
	public void  LogInAs() {
		
		driver =BrowserActions.OpenBrowser("GC", "http://sc-1470.accounts.speareducation.co/login");
		
		SpearLogin spearlogin = PageFactory.initElements(driver, SpearLogin.class);
		UserFlow userFlow = PageFactory.initElements(driver, UserFlow.class);
		
		spearlogin.Login("mkothi@speareducation.com", "Abinika@123");	
		
		userFlow.ClickOnUsers();
		userFlow.SearchUserByID("75108");
		userFlow.ClickSearchButton();
		userFlow.ClickLoginAsICON();
		
		BrowserActions.CloseBrowser();
		
	}

}
