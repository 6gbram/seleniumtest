package TestscriptsAccountsAWS;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import AccountsAWS.Newuser;
import TestAutomation.Helper.BrowserActions;
import TestAutomation.Selenium.SpearLogin;

public class testcaseaddnewuser {
	WebDriver driver;
	
	@Test
	public void  addnewuser() {
		
		driver =BrowserActions.OpenBrowser("GC", "http://sc-1470.accounts.speareducation.co/login");
		
		SpearLogin spearlogin = PageFactory.initElements(driver, SpearLogin.class);
		Newuser Newuser = PageFactory.initElements(driver, Newuser.class);
		
		spearlogin.Login("mkothi@speareducation.com", "Abinika@123");	
		
	    Newuser.ClickOnUsers();
		Newuser.ClickOnNewuser();
		Newuser.firstnameByID("testmktest8auto");
		Newuser.lastnameById("test12345");
		Newuser.useremailById("testmk8.2@testauto.com");
		Newuser.upasswordById("test12345");
		Newuser.ClickOnsubmit();
		
		
		BrowserActions.CloseBrowser();
		
	}
	
	
	
	
	
}
