package TestscriptsAccountsAWS;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import AccountsAWS.PAGEVER;
import TestAutomation.Helper.BrowserActions;

public class Pageverification {
	
	WebDriver driver;
	@Test
	public void pageverification() {
		
		driver =BrowserActions.OpenBrowser("GC", "https://www.speareducation.net/");
		
		PAGEVER  PAGEVER = PageFactory.initElements(driver, PAGEVER.class);
		
		PAGEVER.campuslearninglinkcheck();
		PAGEVER.OnlineEducationlinkcheck();
		PAGEVER.patientEducationlinkcheck();
		PAGEVER.TeamEducationlinkcheck();
		PAGEVER.studyclublinkcheck();
		PAGEVER.Practiceconsultinglinkcheck();
		PAGEVER.SpearFacultylinkcheck();
		PAGEVER.SpearDigestlinkcheck();
		
		BrowserActions.CloseBrowser();
		
	}

}
